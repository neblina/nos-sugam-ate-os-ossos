from mezzanine.conf import register_setting

register_setting(
    name="PAGE_DROPDOWN_TITLE",
    label="Page dropdown title",
    description="What is shown in the button which lists the other pages",
    editable=True,
    default='edições',
)
