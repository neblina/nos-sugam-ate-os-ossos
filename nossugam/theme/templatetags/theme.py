from django import template
from mezzanine.pages.models import RichTextPage

register = template.Library()

@register.simple_tag
def page_title(url):
    try:
        return RichTextPage.objects.get(slug=url).title
    except RichTextPage.DoesNotExist:
        return ""
